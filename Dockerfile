#To build the project
FROM maven:3.5.2-jdk-8 AS build-env
COPY . /usr/src/companyapi
WORKDIR /usr/src/companyapi
#Build app
RUN mvn clean package -DskipTests
ARG JAR_FILE=target/*.jar


#Build runtime image
FROM openjdk:8-jdk-alpine
RUN addgroup -S spring && adduser -S spring -G spring
USER spring:spring
COPY --from=build-env /usr/src/companyapi/target/company-api-0.0.1-SNAPSHOT.jar /app.jar
ENTRYPOINT ["java","-jar","/app.jar"]