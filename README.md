# Company service
This microservice is intended to manage companies.

Basic HTTP-type operations are contemplated.

Connection to RabbitMQ for logger, where this microservice has the role of producer.

# Authors
- [Rodolfo Justiniano](https://gitlab.com/rodolfo.justiniano) as trainer
- [Edgar Balderrama](https://gitlab.com/EdgarBalderrama) as developer
