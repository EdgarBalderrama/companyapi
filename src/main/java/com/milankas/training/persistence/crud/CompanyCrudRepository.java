package com.milankas.training.persistence.crud;

import com.milankas.training.persistence.entity.CompanyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface CompanyCrudRepository extends JpaRepository<CompanyEntity, UUID> {

    List<CompanyEntity> findByName(String name);
}
