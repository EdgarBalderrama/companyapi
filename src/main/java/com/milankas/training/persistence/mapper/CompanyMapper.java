package com.milankas.training.persistence.mapper;

import com.milankas.training.domain.Company;
import com.milankas.training.persistence.entity.CompanyEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

import java.util.List;

@Mapper(componentModel = "spring", uses = {AddressMapper.class})
public interface CompanyMapper {

    @Mappings({
            @Mapping(source = "idCompany", target = "companyId"),
            @Mapping(source = "name", target = "companyName"),
            @Mapping(source = "addressEntity", target = "address"),
    })
    Company toCompany(CompanyEntity companyEntity);
    List<Company> toCompanies(List<CompanyEntity> companyEntities);

    @InheritInverseConfiguration
    CompanyEntity toCompanyEntity(Company company);
}
