package com.milankas.training.persistence.mapper;

import com.milankas.training.domain.Address;
import com.milankas.training.persistence.entity.AddressEntity;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

@Mapper(componentModel = "spring", uses = {CompanyMapper.class})
public interface AddressMapper {

    @Mappings({
            @Mapping(source = "idAddress", target = "addressId"),
    })
    Address toAddress(AddressEntity addressEntity);

    @InheritInverseConfiguration
    AddressEntity toAddressEntity(Address address);
}
