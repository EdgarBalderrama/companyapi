package com.milankas.training.persistence;

import com.milankas.training.domain.Company;
import com.milankas.training.domain.repository.CompanyRepository;
import com.milankas.training.persistence.crud.CompanyCrudRepository;
import com.milankas.training.persistence.entity.CompanyEntity;
import com.milankas.training.persistence.mapper.CompanyMapper;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static org.springframework.data.domain.ExampleMatcher.GenericPropertyMatchers.contains;

@Repository
public class CompanyEntityRepository implements CompanyRepository {

    private final CompanyCrudRepository companyCrudRepository;
    private final CompanyMapper companyMapper;

    public CompanyEntityRepository(CompanyCrudRepository companyCrudRepository, CompanyMapper companyMapper) {
        this.companyCrudRepository = companyCrudRepository;
        this.companyMapper = companyMapper;
    }

    @Override
    public Optional<Company> getCompany(UUID companyId) {
        return companyCrudRepository.findById(companyId).map(companyMapper::toCompany);
    }

    @Override
    public List<Company> findAll(Company company) {
        CompanyEntity companyEntity = companyMapper.toCompanyEntity(company);
        ExampleMatcher matcher = ExampleMatcher.matching().withIgnoreCase();

        Example<CompanyEntity> companyEntityExample = Example.of(companyEntity, matcher);
        List<CompanyEntity> companies = companyCrudRepository.findAll(companyEntityExample);

        return companyMapper.toCompanies(companies);
    }

    @Override
    public Company save(Company company) {
        CompanyEntity companyEntity = companyMapper.toCompanyEntity(company);
        return companyMapper.toCompany(companyCrudRepository.save(companyEntity));
    }

    @Override
    public void delete(UUID companyId) {
        companyCrudRepository.deleteById(companyId);
    }
}
