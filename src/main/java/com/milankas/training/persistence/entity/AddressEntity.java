package com.milankas.training.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "addresses")
@Getter
@Setter
public class AddressEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_address")
    private UUID idAddress;

    @Column(name = "address_line_1")
    private String addressLine1;

    @Column(name = "address_line_2")
    private String addressLine2;

    private String state;
    private String city;

    @Column(name = "zip_code")
    private String zipCode;

    @Column(name = "country_code")
    private String countryCode;
}
