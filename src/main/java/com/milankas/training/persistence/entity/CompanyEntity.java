package com.milankas.training.persistence.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "companies")
@Getter
@Setter
public class CompanyEntity {

    @Id
    @GeneratedValue
    @Column(name = "id_company")
    private UUID idCompany;
    private String name;

    @ManyToOne(cascade = CascadeType.ALL)
    private AddressEntity addressEntity;
}
