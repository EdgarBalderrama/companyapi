package com.milankas.training.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
public class Address {

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private UUID addressId;

    @Size(min = 5, max = 50)
    private String addressLine1;

    @Size(min = 5, max = 50)
    private String addressLine2;

    @Size(min = 2, max = 30)
    private String state;

    @Size(min = 2, max = 60)
    private String city;

    @Min(100)
    @Max(99999999)
    private Integer zipCode;

    @Pattern(regexp = "^[A-Z]{2}$", message = "country code must have only two capital letters")
    private String countryCode;
}
