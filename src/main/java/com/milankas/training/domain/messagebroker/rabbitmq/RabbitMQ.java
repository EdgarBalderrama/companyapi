package com.milankas.training.domain.messagebroker.rabbitmq;

import com.milankas.training.domain.messagebroker.MessageBroker;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RabbitMQ implements MessageBroker {

    private final RabbitTemplate rabbitTemplate;
    private final String exchange;
    private final String routingKey;

    public RabbitMQ(RabbitTemplate rabbitTemplate,
                    @Value("${rabbitmq.exchange}")String exchange,
                    @Value("${rabbitmq.routingKey}") String routingKey) {
        this.rabbitTemplate = rabbitTemplate;
        this.exchange = exchange;
        this.routingKey = routingKey;
    }

    @Override
    public void send(Object message) {
        rabbitTemplate.setExchange(exchange);
        rabbitTemplate.setRoutingKey(routingKey);
        rabbitTemplate.convertAndSend("ComApi" + message);
    }
}
