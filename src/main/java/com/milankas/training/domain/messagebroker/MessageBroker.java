package com.milankas.training.domain.messagebroker;

public interface MessageBroker {
    void send(Object message);
}
