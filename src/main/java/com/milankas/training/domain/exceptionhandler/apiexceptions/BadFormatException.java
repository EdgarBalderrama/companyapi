package com.milankas.training.domain.exceptionhandler.apiexceptions;

public class BadFormatException extends RuntimeException{

    public BadFormatException(String message){
        super(message);
    }
}
