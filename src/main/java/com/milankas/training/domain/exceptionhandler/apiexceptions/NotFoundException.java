package com.milankas.training.domain.exceptionhandler.apiexceptions;

public class NotFoundException extends RuntimeException{

    public NotFoundException(String message){
        super(message);
    }
}
