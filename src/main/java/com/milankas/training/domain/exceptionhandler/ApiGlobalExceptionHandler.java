package com.milankas.training.domain.exceptionhandler;

import com.milankas.training.domain.exceptionhandler.apiexceptions.BadFormatException;
import com.milankas.training.domain.exceptionhandler.apiexceptions.NotFoundException;
import com.milankas.training.domain.exceptionhandler.models.ApiError;
import com.milankas.training.domain.exceptionhandler.models.ApiErrorDetail;
import com.milankas.training.domain.messagebroker.MessageBroker;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.util.WebUtils;

import java.util.ArrayList;
import java.util.List;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class ApiGlobalExceptionHandler extends ResponseEntityExceptionHandler {

    private final MessageBroker messageBroker;

    public ApiGlobalExceptionHandler(MessageBroker messageBroker) {
        this.messageBroker = messageBroker;
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<ApiErrorDetail> errorDetails = new ArrayList<>();
        String errorText = "";
        for (FieldError error : ex.getBindingResult().getFieldErrors()) {
            errorDetails.add(new ApiErrorDetail(error.getField(), error.getDefaultMessage()));
            errorText += error.getField() + " " + error.getDefaultMessage() + ". ";
        }
        for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errorDetails.add(new ApiErrorDetail(error.getObjectName(), error.getDefaultMessage()));
        }
        messageBroker.send("Error: Status Code: 400 Bad Request Message: " + errorText);

        ApiError apiError = new ApiError(ex.getLocalizedMessage(), HttpStatus.BAD_REQUEST, errorDetails);
        return handleExceptionInternal(ex, apiError, headers, apiError.getStatus(), request);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception ex, Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
        if (HttpStatus.INTERNAL_SERVER_ERROR.equals(status)) {
            request.setAttribute(WebUtils.ERROR_EXCEPTION_ATTRIBUTE, ex, WebRequest.SCOPE_REQUEST);
        }
        return new ResponseEntity<>(body, headers, status);
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        String error = ex.getParameterName() + " parameter is missing";

        List<ApiErrorDetail> errorDetails = new ArrayList<>();
        errorDetails.add(new ApiErrorDetail(ex.getParameterName(), error));
        messageBroker.send("Error: Status Code: 400 Bad Request");

        ApiError apiError = new ApiError(ex.getLocalizedMessage(), HttpStatus.BAD_REQUEST, errorDetails);
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(MethodArgumentTypeMismatchException ex, WebRequest request) {
        String error = ex.getName() + " should be of type " + ex.getRequiredType().getName();
        messageBroker.send("Error: Status Code: 400 Bad Request. HTTP Method: Get. Message: " + ex.getName() + " should be of type " + ex.getRequiredType().getName());

        ApiError apiError = new ApiError(error, HttpStatus.BAD_REQUEST);
        return new ResponseEntity<Object>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<ApiError> handleNotFoundException(NotFoundException ex) {
        ApiError apiError = new ApiError(ex.getMessage(), HttpStatus.NOT_FOUND);
        messageBroker.send("Error: Status Code: 400 Bad Request Message: " + ex.getMessage());
        return new ResponseEntity<ApiError>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({BadFormatException.class})
    public ResponseEntity<ApiError> handleBadFormatException(BadFormatException ex) {
        ApiError apiError = new ApiError(ex.getMessage(), HttpStatus.BAD_REQUEST);
        messageBroker.send("Error: Status Code: 400 Bad Request Message: " + ex.getMessage());
        return new ResponseEntity<ApiError>(apiError, new HttpHeaders(), apiError.getStatus());
    }

    @ExceptionHandler({ Exception.class, RuntimeException.class })
    public ResponseEntity<ApiError> handleAnyException(Exception exception){
        ApiError apiError = new ApiError(exception.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity<ApiError>(apiError, new HttpHeaders(), apiError.getStatus());
    }
}
