package com.milankas.training.domain.service;

import com.milankas.training.domain.Company;
import com.milankas.training.domain.exceptionhandler.apiexceptions.NotFoundException;
import com.milankas.training.domain.messagebroker.MessageBroker;
import com.milankas.training.domain.repository.CompanyRepository;
import com.milankas.training.domain.utils.MyBeansUtil;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CompanyService {

    private final CompanyRepository companyRepository;
    private final MessageBroker messageBroker;

    public CompanyService(CompanyRepository companyRepository, MessageBroker messageBroker) {
        this.companyRepository = companyRepository;
        this.messageBroker = messageBroker;
    }

    public Company getCompany(UUID companyId){
        Optional<Company> company = companyRepository.getCompany(companyId);
        if (!company.isPresent()){
            throw new NotFoundException("Company does not exists");
        }
        return company.get();
    }

    public Company getCompanyForRabbitMessage(UUID companyId){
        Optional<Company> company = companyRepository.getCompany(companyId);
        if (!company.isPresent()){
            throw new NotFoundException("Company does not exists");
        }
        messageBroker.send("Status Code: 200 ok. HTTP Method: Get. Message: Get company by ID: " + companyId);
        return company.get();
    }

    public List<Company> findBy(String name) {
        Company company = new Company();
        company.setCompanyName(name);

        messageBroker.send("Status Code: 200 ok. HTTP Method: Get. Message: Find companies");
        return companyRepository.findAll(company);
    }

    public Company save(Company company){
        messageBroker.send("Status Code: 200 ok. HTTP Method: Post. Message: New company saved");
        return companyRepository.save(company);
    }

    public Company update(UUID id, Company userCompany){
        messageBroker.send("Status Code: 200 ok. HTTP Method: Patch. Message: Company updated");

        Company company = getCompany(id);
        MyBeansUtil<Company> cloner = new MyBeansUtil<>();
        company = cloner.copyNonNullProperties(company, userCompany);

        return companyRepository.save(company);
    }

    public void delete(UUID companyId){
        Company company = getCompany(companyId);
        messageBroker.send("Status Code: 200 ok. HTTP Method: Delete. Message: Company deleted");
        companyRepository.delete(company.getCompanyId());
    }
}
