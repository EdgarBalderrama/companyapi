package com.milankas.training.domain.repository;

import com.milankas.training.domain.Company;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CompanyRepository {

    List<Company> findAll(Company company);
    Optional<Company> getCompany(UUID companyId);
    Company save(Company company);
    void delete(UUID companyId);
}
