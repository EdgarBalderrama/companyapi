package com.milankas.training.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.UUID;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Company {

    private UUID companyId;

    @Size(min = 2, max = 20)
    private String companyName;

    @Valid
    private Address address;
}
