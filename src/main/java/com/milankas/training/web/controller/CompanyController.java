package com.milankas.training.web.controller;

import com.milankas.training.domain.Company;
import com.milankas.training.domain.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/companies")
public class CompanyController {

    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping("/{id}")
    public Company getCompany(@PathVariable("id") UUID companyId){
        return companyService.getCompanyForRabbitMessage(companyId);
    }

    @GetMapping
    public List<Company> getByParams(@RequestParam(value = "name", required = false) String name) {
        return companyService.findBy(name);
    }

    @PostMapping
    public Company save(@Valid @RequestBody Company company){
        return companyService.save(company);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id")UUID companyId){
        companyService.delete(companyId);
    }

    @PatchMapping("/{id}")
    public Company update(@PathVariable("id") UUID id, @Valid @RequestBody Company userCompany){
        return companyService.update(id, userCompany);
    }
}
